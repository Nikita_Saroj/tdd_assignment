class StringCalculator

  def add(str)
    if str.start_with?("//")
      delimiter, numbers = str.split("\n")
      delimiter = delimiter[2]
    else
      delimiter = ","
    end
    numbers = str.split(/[\s#{delimiter}']/)
    sum = 0
    numbers.each do |num|
      if num.to_i.negative?
        raise ArgumentError, "negative numbers not allowed #{num}"
      else
        sum += num.to_i
      end
    end
    sum
  end

end