require './string_calculator.rb'

describe StringCalculator do

  before(:each) do 
    @string_calculator = StringCalculator.new 
  end

  context 'Adding string numbers' do
    it "should return sum as 0 in case of empty string" do
      sum = @string_calculator.add("")
      expect(sum).to eql(0)
    end

    it "should return 1 in case of string 1" do
      sum = @string_calculator.add("1")
      expect(sum).to eql(1)
    end

    it "should return 2 in case of string 2" do
      sum = @string_calculator.add("2")
      expect(sum).to eql(2)
    end

    it "should add string numbers 1, 5 and return sum as 6" do
      sum = @string_calculator.add("1,5")
      expect(sum).to eql(6)
    end

    it "should add string numbers 2, 3 and return sum as 5" do
      sum = @string_calculator.add("2,3")
      expect(sum).to eql(5)
    end

    it "should add string with input as 2, 3, 5 and return sum as 10" do
      sum = @string_calculator.add("2,3,5")
      expect(sum).to eql(10)
    end

    it "should handle new lines passed in input string" do
      sum = @string_calculator.add("1\n2,3")
      expect(sum).to eql(6)
    end

    it "should support string input seperated with ; delimiter" do
      sum = @string_calculator.add("//;\n1;2")
      expect(sum).to eql(3)
    end

    it "should support string input seperated with | delimiter" do
      sum = @string_calculator.add("//|\n1|2")
      expect(sum).to eql(3)
    end

    it "should throw an error for negative number" do
      expect { @string_calculator.add("-1,2") }.to raise_error(ArgumentError, "negative numbers not allowed -1")
    end
  end
end
